#!/bin/bash

branchName="develop"

if [ -d ere-publisher ]; then
    echo "ere publisher already present"
    (cd ere-publisher; git fetch origin;git checkout $branchName; git pull origin $branchName)
else
    git clone -b $branchName https://gitlab.com/vlead-projects/experiments/ere/publish/ere-publisher.git
    (cd ere-publisher)
fi

if [ -L pub-make ]; then
    echo "symlinked makefile already present"
else 
    ln -sf ere-publisher/makefile pub-make
fi
